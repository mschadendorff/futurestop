import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SurroundingRoutingModule } from './surrounding-routing.module';
import { SurroundingComponent } from './surrounding.component';
import { ProviderComponent } from './provider/provider.component';
import { OverviewComponent } from './overview/overview.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {RouterModule} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";
import {NgxMapboxGLModule} from "ngx-mapbox-gl";


@NgModule({
  declarations: [SurroundingComponent, ProviderComponent, OverviewComponent],
  imports: [
    CommonModule,
    SurroundingRoutingModule,
    FlexLayoutModule,
    RouterModule,
    MatButtonModule,
    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1IjoibXNjaGFkZW5kb3JmZiIsImEiOiJjazU2anNibmswNGVpM21wZ3JlbXJhcDVwIn0.nYtZKmRTN17TQ06N0jAHTg'
    })
  ]
})
export class SurroundingModule { }
