import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SurroundingComponent} from "./surrounding.component";
import {ProviderComponent} from "./provider/provider.component";
import {OverviewComponent} from "./overview/overview.component";


const routes: Routes = [
  {
    path: 'surrounding',
    component: SurroundingComponent,
    children: [
      {
        path: '',
        component: ProviderComponent
      },
      {
        path: 'overview',
        component: OverviewComponent
      }

    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SurroundingRoutingModule { }
