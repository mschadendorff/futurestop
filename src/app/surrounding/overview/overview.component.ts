import { Component, OnInit } from '@angular/core';
import * as mapboxgl from "mapbox-gl";
import {CommunicationService} from "../../services/communication.service";
import {Provider} from "../../destination/connections/model/provider";

@Component({
  selector: 'surrounding-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  map: mapboxgl.Map;
  userLocationControl: mapboxgl.GeolocateControl;

  constructor(private communicationService: CommunicationService) {}

  ngOnInit() {
  }

  mapLoaded(map: mapboxgl.Map): void {
    let that = this;
    this.map = map;

    this.userLocationControl = new mapboxgl.GeolocateControl({
      positionOptions: {
        enableHighAccuracy: true
      },
      showUserLocation: true
    });

    this.userLocationControl.on("geolocate", function (data) {
      map.flyTo({
        center: [data.coords.longitude, data.coords.latitude],
        zoom: 12,
        curve: 0.5
      });
    });

    let selectedProviders: Provider[] = this.communicationService.getSelectedProviders();
    let points = this.communicationService.getPoints();

    for (let i = 0; i < selectedProviders.length * 150; i++) {
      let index = Math.floor(i / 150);

      let el = document.createElement('div');
      el.style.backgroundColor = selectedProviders[index].color;
      el.style.backgroundSize = '80% 80%';
      el.style.backgroundRepeat = 'no-repeat';
      el.style.backgroundPosition = 'center';
      el.style.width = '30px';
      el.style.height = '30px';
      el.style.borderRadius = '50%';
      el.style.backgroundImage = 'url("/assets/img/vehicles/' + selectedProviders[index].key + '.png")';

      new mapboxgl.Marker(el)
        .setLngLat(points[i].geometry.coordinates)
        .addTo(that.map);
    }

    this.map.addControl(this.userLocationControl, 'top-right');

    this.map.addControl(new mapboxgl.NavigationControl(), 'bottom-right');

    setTimeout(() => {
      this.userLocationControl.trigger();
    }, 100);
  }
}
