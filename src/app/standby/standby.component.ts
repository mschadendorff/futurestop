import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl} from "@angular/forms";

@Component({
  selector: 'app-standby',
  templateUrl: './standby.component.html',
  styleUrls: ['./standby.component.scss']
})
export class StandbyComponent implements OnInit {
  time: Date;
  constructor() { }

  ngOnInit() {
    this.time = new Date();

    let that = this;

    setInterval(() => {
      that.time = new Date();
    }, 1000);

  }

}
