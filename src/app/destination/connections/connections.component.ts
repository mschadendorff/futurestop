import {Component, OnInit, ViewChild} from '@angular/core';
import {Connection} from "./model/connection";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {Provider} from "./model/provider";
import {CommunicationService} from "../../services/communication.service";
import * as mapboxgl from "mapbox-gl";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import * as geojson from 'geojson';

@Component({
  selector: 'destination-connections',
  templateUrl: './connections.component.html',
  styleUrls: ['./connections.component.scss']
})
export class ConnectionsComponent implements OnInit {
  public connections: Connection[] = [];
  displayedColumns: string[] = ['time', 'steps', 'duration'];
  dataSource = new MatTableDataSource<Connection>(this.connections);
  providers: Provider[];
  map: mapboxgl.Map;
  selectedRowIndex: number = 0;
  startMarker;
  middleMarker;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private communicationService: CommunicationService, private http: HttpClient) {
    this.providers = this.communicationService.getAllProviders();
    let count = this.getRandomInt(5, 10);
    this.selectedRowIndex = count;
    this.connections = this.createRandomData(count);
    this.dataSource = new MatTableDataSource<Connection>(this.connections);
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.communicationService.setNext("/destination/payMethod");
    this.dataSource.paginator = this.paginator;
  }

  createRandomData(count: number): Connection[] {
    let connections: Connection[] = [];
    let lastDate = null;
    while (count > 0) {
      let allSteps = [...this.communicationService.getSelectedProviders()];
      let duration = this.getRandomInt(20, 40);
      let from = new Date();

      if (lastDate !== null) {
        from = new Date(lastDate.getTime() + this.getRandomInt(5, 5) * 60000);
      }
      let until = new Date(from.getTime() + duration * 60000);

      lastDate = until;

      let stepCount = 2;
      let steps: Provider[] = Array();
      let i = 0;
      while (i < stepCount) {
        let index = this.getRandomInt(0, this.communicationService.getSelectedProviders().length - i);
        steps.push(allSteps[index]);
        allSteps.splice(index, 1);
        i++;
      }

      connections.push({
        id: count,
        from: from,
        until: until,
        steps: steps,
        duration: duration
      });

      count--;
    }

    return connections;
  }

  getRandomInt(min: number, max: number): number {
    return Math.floor((Math.random() * max) + min);
  }

  mapLoaded(map: mapboxgl.Map): void {
    let that = this;
    this.map = map;

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      })
    };

    let start = this.communicationService.getStart();
    let destination = this.communicationService.getDestination();
    let body = "coordinates=" + start[0] + "," + start[1] + ";" + destination[0] + "," + destination[1] + "&geometries=geojson&overview=full&steps=false&alternatives=false";

    this.http.post<any>(
      "https://api.mapbox.com/directions/v5/mapbox/cycling?access_token="+ mapboxgl.accessToken,
      body,
      httpOptions)
      .subscribe(resp => {
        let stepsCount = resp.routes[0].geometry.coordinates.length;
        let coords1 = [];
        let coords2 = [];

        let i = 0;

        let marker = {
          type: 'FeatureCollection',
          features: [{
            type: 'Feature',
            geometry: {
              type: 'Point',
              coordinates: resp.routes[0].geometry.coordinates[i]
            },
            properties: {
              title: 'Mapbox',
              description: 'Washington, D.C.',
              position: 'start'
            }
          }]
        };

        for(; i < stepsCount / 2; i++) {
          coords1.push(resp.routes[0].geometry.coordinates[i])
        }

        marker.features.push({
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: resp.routes[0].geometry.coordinates[i]
          },
          properties: {
            title: '',
            description: 'Washington, D.C.',
            position: 'middle'
          }
        });

        this.map.setCenter(resp.routes[0].geometry.coordinates[i]);

        for(; i < stepsCount; i++) {
          coords2.push(resp.routes[0].geometry.coordinates[i - 1])
        }

        coords2.push(resp.routes[0].geometry.coordinates[stepsCount - 1]);

        marker.features.push({
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: resp.routes[0].geometry.coordinates[stepsCount - 1]
          },
          properties: {
            title: 'Mapbox',
            description: 'Washington, D.C.',
            position: 'end'
          }
        });

        that.map.addLayer({
          'id': 'route1',
          'type': 'line',
          'source': {
            type: 'geojson',
            data: <geojson.Feature>{
              type: "Feature",
              properties: {},
              geometry: {
                type: "LineString",
                coordinates: coords1
              }
            }
          },
          'layout': {
            'line-join': 'round',
            'line-cap': 'round'
          },
          'paint': {
            'line-color': this.dataSource.data[0].steps[0].color,
            'line-width': 8,
            'line-opacity': 0.8
          }
        });

        that.map.addLayer({
          'id': 'route2',
          'type': 'line',
            'source': {
              type: 'geojson',
              data: <geojson.Feature>{
                type: "Feature",
                properties: {},
                geometry: {
                  type: "LineString",
                  coordinates: coords2
                }
              }
            },
          'layout': {
            'line-join': 'round',
            'line-cap': 'round'
          },
          'paint': {
            'line-color': this.dataSource.data[0].steps[1].color,
            'line-width': 8,
            'line-opacity': 0.8
          }
        });

        marker.features.forEach(function(marker, index) {
          let el = document.createElement('div');

          el.style.backgroundSize = '80% 80%';
          el.style.backgroundRepeat = 'no-repeat';
          el.style.backgroundPosition = 'center';
          el.style.width = '30px';
          el.style.height = '30px';
          el.style.borderRadius = '50%';

          if (index === 0) {
            that.startMarker = el;
            el.style.backgroundImage = 'url("/assets/img/vehicles/' + that.dataSource.data[0].steps[0].key + '.png")';
            el.style.backgroundColor = that.dataSource.data[0].steps[0].color;
          }
          else if (index === 1) {
            that.middleMarker = el;
            el.style.backgroundImage = 'url("/assets/img/vehicles/' + that.dataSource.data[0].steps[1].key + '.png")';
            el.style.backgroundColor = that.dataSource.data[0].steps[1].color;
          }
          else {
            el.style.backgroundColor = '#DC6E46';
            el.style.backgroundImage = 'url("/assets/img/vehicles/destination_reached.png")';
          }

          new mapboxgl.Marker(el)
            .setLngLat(marker.geometry.coordinates)
            .addTo(that.map);
        });
      });

    this.map.addControl(new mapboxgl.NavigationControl(), 'bottom-right');
  }

  setRouteColor(connection: Connection): void {
    this.map.setPaintProperty('route1', 'line-color', connection.steps[0].color);
    this.map.setPaintProperty('route2', 'line-color', connection.steps[1].color);

    this.startMarker.style.backgroundImage = 'url("/assets/img/vehicles/' + connection.steps[0].key + '.png")';
    this.middleMarker.style.backgroundImage = 'url("/assets/img/vehicles/' + connection.steps[1].key + '.png")';
    this.startMarker.style.backgroundColor = connection.steps[0].color;
    this.middleMarker.style.backgroundColor = connection.steps[1].color;

    this.selectedRowIndex = connection.id;
  }

}
