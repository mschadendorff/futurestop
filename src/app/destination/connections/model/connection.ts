import {Provider} from "./provider";

export class Connection {
  id: number;
  from: Date;
  until: Date;
  steps: Provider[];
  duration: number;
}
