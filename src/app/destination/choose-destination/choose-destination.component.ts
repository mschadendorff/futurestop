import {Component, ElementRef, OnInit} from '@angular/core';
import {CommunicationService} from "../../services/communication.service";
import {
  MatKeyboardComponent,
  MatKeyboardRef,
  MatKeyboardService
} from "@ngx-material-keyboard/core";
import * as mapboxgl from 'mapbox-gl';
import * as Geocoder from '@mapbox/mapbox-gl-geocoder/'
import {AbstractControl, FormControl} from "@angular/forms";

@Component({
  selector: 'destination-choose-destination',
  templateUrl: './choose-destination.component.html',
  styleUrls: ['./choose-destination.component.scss']
})
export class ChooseDestinationComponent implements OnInit {
  keyboardRef: MatKeyboardRef<MatKeyboardComponent>[] = new Array(2);
  start: number[] = [];
  destination: number[] = [];
  destinationControl: Geocoder;
  startControl: Geocoder;
  userLocationControl: mapboxgl.GeolocateControl;
  error: string = "Bitte wähle ein Ziel";
  map: mapboxgl.Map;

  constructor(private communicationService: CommunicationService,
              private keyboardService: MatKeyboardService) {}

  ngOnInit() {
    this.communicationService.setNext("/destination/providers");
    this.communicationService.setError(this.error);
  }

  mapLoaded(map: mapboxgl.Map): void {
    let that = this;
    this.map = map;
    this.userLocationControl = new mapboxgl.GeolocateControl({
      positionOptions: {
        enableHighAccuracy: true
      },
      showUserLocation: true
    });

    this.startControl = new Geocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl: mapboxgl,
      placeholder: "Start: Akt. Standpunkt",
      countries: 'de',
      marker: {
        color: 'orange'
      },
      zoom: 13,
      language: 'de-DE'
    });

    this.destinationControl = new Geocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl: mapboxgl,
      placeholder: "Ziel",
      countries: 'de',
      marker: {
        color: 'orange'
      },
      zoom: 13,
      language: 'de-DE'
    });

    this.startControl.on("results", function (result) {
      that.start = [...result.features[0].center];
      that.communicationService.setStart(that.start);
    });

    this.destinationControl.on("results", function (result) {
      that.destination = [...result.features[0].center];
      that.communicationService.setDestination(that.destination);
      that.communicationService.setError("");
    });

    this.destinationControl.on("clear", function (result) {
      that.communicationService.setError(that.error);
      that.communicationService.setDestination([]);
    });

    this.userLocationControl.on("geolocate", function (data) {
      that.start[0] = data.coords.longitude;
      that.start[1] = data.coords.latitude;

      that.communicationService.setStart(that.start);
      map.flyTo({
        center: [data.coords.longitude, data.coords.latitude],
        zoom: 12,
        curve: 0.5
      });
    });


    this.map.addControl(this.startControl, 'top-left');
    this.map.addControl(this.destinationControl, 'top-right');
    this.map.addControl(this.userLocationControl, 'top-right');

    this.map.addControl(new mapboxgl.NavigationControl(), 'bottom-right');

    setTimeout(function () {
      that.userLocationControl.trigger();

      let inputs: HTMLCollection = document.getElementsByClassName("mapboxgl-ctrl-geocoder--input");

      let inputRefs: ElementRef[] = [];
      let fakeModels: AbstractControl[] = [];

      for(let i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener("focus", (event) => {
          that.keyboardRef[i] = that.keyboardService.open('de', {
            darkTheme: false,
            duration: 100,
            isDebug: false
          });

          inputRefs[i] = new ElementRef(inputs[i]);
          fakeModels[i] = new FormControl();

          fakeModels[i].setValue((inputs[i] as HTMLInputElement).value);
          fakeModels[i].valueChanges.subscribe((value) => {
            (inputs[i] as HTMLInputElement).value = value;

            let keydown = new KeyboardEvent('keydown', {
              bubbles: true,
              cancelable: true
            });

            (inputs[i] as HTMLInputElement).dispatchEvent(keydown);
          });

          that.keyboardRef[i].instance.attachControl(fakeModels[i]);
          that.keyboardRef[i].instance.setInputInstance(inputRefs[i]);
        });

        inputs[i].addEventListener("blur", (event) => {
          that.keyboardRef[i].dismiss();
        });
      }

      (inputs[1] as HTMLInputElement).focus();
    }, 100);
  }
}
