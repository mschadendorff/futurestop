import { Component, OnInit } from '@angular/core';
import {CommunicationService} from "../../services/communication.service";
import {Provider} from "../connections/model/provider";

@Component({
  selector: 'destination-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.scss']
})
export class ProviderComponent implements OnInit {
  allProviders: Provider[];
  selectedProviders: Provider[];
  error: string = "Bitte wähle mind. zwei Anbieter";

  constructor(private communicationService: CommunicationService) { }

  ngOnInit() {
    this.communicationService.setNext("/destination/connections");
    this.allProviders = this.communicationService.getAllProviders();
    this.selectedProviders = this.communicationService.getSelectedProviders();
    if (this.selectedProviders.length < 2) {
      this.communicationService.setError(this.error);
    }
    else {
      this.communicationService.setError("");
    }
  }

  toggleProvider(provider: Provider): void {
    let selectedProviders = this.communicationService.getSelectedProviders();
    let index = selectedProviders.findIndex((elem) => provider.key === elem.key);

    if (index === -1) {
      selectedProviders.push(provider);
      if (selectedProviders.length > 1) {
        this.communicationService.setError("");
      }
    }
    else {
      selectedProviders.splice(index, 1);

      if (selectedProviders.length < 2) {
        this.communicationService.setError(this.error);
      }
    }
  }

  isSelectedProvider(provider: Provider): boolean {
    let selectedProviders = this.communicationService.getSelectedProviders();
    return selectedProviders.findIndex((elem) => provider.key === elem.key) !== -1;
  }
}
