import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'destination-bye',
  templateUrl: './bye.component.html',
  styleUrls: ['./bye.component.scss']
})
export class ByeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    let that = this;

  setTimeout(() => {
      that.router.navigate(["/destination", {}]);
    }, 5000);
  }

}
