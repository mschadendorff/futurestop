import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';

import { DestinationRoutingModule } from './destination-routing.module';
import { DestinationComponent } from './destination.component';
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatInputModule } from "@angular/material/input";
import {IKeyboardLayouts, keyboardLayouts, MAT_KEYBOARD_LAYOUTS, MatKeyboardModule} from '@ngx-material-keyboard/core';
import { ConnectionsComponent } from './connections/connections.component';
import {MatDividerModule} from "@angular/material/divider";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorIntl, MatPaginatorModule} from "@angular/material/paginator";
import {MatPaginatorIntlDe} from "../providers/MatPaginatorIntlDe";
import {RouterModule} from "@angular/router";
import {NgxMapboxGLModule} from "ngx-mapbox-gl";
import {HttpClientModule} from "@angular/common/http";
import { ProviderComponent } from './provider/provider.component';
import {MatButtonModule} from "@angular/material/button";
import { ChooseDestinationComponent } from './choose-destination/choose-destination.component';
import { PaymethodComponent } from './paymethod/paymethod.component';
import { PaymentComponent } from './payment/payment.component';
import { ByeComponent } from './bye/bye.component';

@NgModule({
  declarations: [DestinationComponent, ConnectionsComponent, ProviderComponent, ChooseDestinationComponent, PaymethodComponent, PaymentComponent, ByeComponent],
  imports: [
    CommonModule,
    DestinationRoutingModule,
    FlexLayoutModule,
    MatInputModule,
    MatKeyboardModule,
    MatTableModule,
    MatDividerModule,
    MatPaginatorModule,
    RouterModule,
    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1IjoibXNjaGFkZW5kb3JmZiIsImEiOiJjazU2anNibmswNGVpM21wZ3JlbXJhcDVwIn0.nYtZKmRTN17TQ06N0jAHTg'
    }),
    HttpClientModule,
    MatButtonModule
  ],
  providers:[
    DatePipe,
    {provide: MatPaginatorIntl, useClass: MatPaginatorIntlDe}
  ],
  exports: [DestinationComponent]
})
export class DestinationModule { }
