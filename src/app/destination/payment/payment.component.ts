import { Component, OnInit } from '@angular/core';
import {CommunicationService} from "../../services/communication.service";
import {Payment} from "../../services/model/payment";
import {Router} from "@angular/router";

@Component({
  selector: 'destination-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  payment: Payment;

  constructor(private communicationService: CommunicationService,
              private router: Router) { }

  ngOnInit() {
    this.payment = this.communicationService.getPayment();

    let that = this;

    setTimeout(() => {
      that.router.navigate(["/destination/bye", {}]);
    }, 8000);
  }

}
