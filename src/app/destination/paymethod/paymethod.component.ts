import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {CommunicationService} from "../../services/communication.service";

@Component({
  selector: 'destination-paymethod',
  templateUrl: './paymethod.component.html',
  styleUrls: ['./paymethod.component.scss']
})
export class PaymethodComponent implements OnInit {

  constructor(private communicationService: CommunicationService,
              private router: Router) { }

  ngOnInit() {
  }

  choosePayMethod(payMethod: string): void {
    this.communicationService.setPayMethod(payMethod);
    this.router.navigate(["/destination/payment", {}]);
  }
}
