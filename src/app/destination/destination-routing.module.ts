import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ConnectionsComponent} from "./connections/connections.component";
import {ProviderComponent} from "./provider/provider.component";
import {DestinationComponent} from "./destination.component";
import {ChooseDestinationComponent} from "./choose-destination/choose-destination.component";
import {PaymethodComponent} from "./paymethod/paymethod.component";
import {PaymentComponent} from "./payment/payment.component";
import {ByeComponent} from "./bye/bye.component";

const routes: Routes = [
  {
    path: "destination",
    component: DestinationComponent,
    children: [{
      path: '',
      component: ChooseDestinationComponent
      },
      {
        path: "providers",
        component: ProviderComponent
      },
      {
        path: "connections",
        component: ConnectionsComponent
      },
      {
        path: "payMethod",
        component: PaymethodComponent
      },
      {
        path: "payment",
        component: PaymentComponent
      },
      {
        path: "bye",
        component: ByeComponent
      }]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DestinationRoutingModule { }
