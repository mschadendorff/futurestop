import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from "@angular/material/tabs";
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";

import { InteractionComponent } from './interaction/interaction.component';
import { FeatureComponent } from './feature/feature.component';
import {RouterModule} from "@angular/router";


@NgModule({
  declarations: [InteractionComponent, FeatureComponent],
  imports: [
    CommonModule,
    MatTabsModule,
    FlexLayoutModule,
    MatIconModule,
    MatButtonModule,
    RouterModule
  ],
  entryComponents: [InteractionComponent]
})
export class StartModule { }
