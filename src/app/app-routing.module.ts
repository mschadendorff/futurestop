import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InteractionComponent} from "./start/interaction/interaction.component";
import {FeatureComponent} from "./start/feature/feature.component";
import {SurroundingComponent} from "./surrounding/surrounding.component";
import {HelpComponent} from "./help/help.component";
import {StandbyComponent} from "./standby/standby.component";

const routes: Routes = [
  {path: "interaction", component: InteractionComponent},
  {path: "feature", component: FeatureComponent},
  {path: "help", component: HelpComponent},
  {path: "standby", component: StandbyComponent},
  {path:"", redirectTo: "/interaction", pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
