import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StartModule } from "./start/start.module";
import { MatTabsModule } from "@angular/material/tabs";
import { FlexLayoutModule } from "@angular/flex-layout";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {DestinationModule} from "./destination/destination.module";
import {SurroundingModule} from "./surrounding/surrounding.module";
import {HelpModule} from "./help/help.module";
import {DatePipe} from "@angular/common";
import { StandbyComponent } from './standby/standby.component';

@NgModule({
  declarations: [
    AppComponent,
    StandbyComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DestinationModule,
    SurroundingModule,
    StartModule,
    HelpModule,
    MatTabsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    AppRoutingModule
  ],
  providers: [
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
