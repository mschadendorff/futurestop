import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {Location} from '@angular/common';
import {CommunicationService} from "./services/communication.service";

enum TabLabel {
  destination = "Zielsuche",
  sourrounding = "Umgebung",
  help = "Hilfe"
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  @ViewChild('tabContainer', {static: false}) tabContainer: ElementRef;
  @ViewChild('errorMessage', {static: false}) errorMessage: ElementRef;
  navLinks: any[];
  isBackButtonVisible: string = 'hidden';
  isNextButtonVisible: string = 'hidden';
  isTabBarVisible: string = 'hidden';
  todayDate : Date = new Date();
  standbyTimeout;
  timeoutTime: number = 300000;

  constructor(private router: Router, private location: Location, private communicationService: CommunicationService) {
    this.navLinks = [
      {path: "/destination", label: TabLabel.destination, icon: "/assets/img/destination.png"},
      {path: "/surrounding", label: TabLabel.sourrounding, icon: "/assets/img/surrounding.png"},
      {path: "/help", label: TabLabel.help, icon: "/assets/img/help.png"}
    ];
  }

  ngOnInit(): void {
    let that = this;
    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          switch (this.router.url) {
            case "/interaction":
            case "/standby":
            case "/destination/bye":
              this.isBackButtonVisible = 'hidden';
              this.isNextButtonVisible = 'hidden';
              this.isTabBarVisible = 'hidden';
              break;
            case "/feature":
              this.isBackButtonVisible = 'visible';
              this.isNextButtonVisible = 'hidden';
              this.isTabBarVisible = 'hidden';
              break;
            case "/destination/payMethod":
            case "/surrounding/overview":
            case "/destination/payment":
            case "/help":
            case "/help/choose-video":
            case "/help/video":
              this.isBackButtonVisible = 'visible';
              this.isNextButtonVisible = 'hidden';
              this.isTabBarVisible = 'visible';
              break;
            case "/destination":
            case "/surrounding":
            case "/destination/providers":
            case "/destination/connections":
            case "/destination/choose-connection":
              this.isBackButtonVisible = 'visible';
              this.isNextButtonVisible = 'visible';
              this.isTabBarVisible = 'visible';
              break;
          }

          clearTimeout(that.standbyTimeout);
          this.standbyTimeout = setTimeout(() => {
            this.router.navigate(["/standby", {}]);
          }, that.timeoutTime)
        }
    });

    this.standbyTimeout = setTimeout(() => {
      this.router.navigate(["/standby", {}]);
    }, this.timeoutTime)
  }

  backClicked() {
    this.communicationService.setError("");
    this.errorMessage.nativeElement.innerText = this.communicationService.getError();
    this.location.back();
  }

  nextClicked() {
    if (this.communicationService.getNext() != "") {
      if (this.communicationService.getError() === "") {
        this.router.navigate([this.communicationService.getNext(), {}]);
        this.communicationService.setError("");
      }
      this.errorMessage.nativeElement.innerText = this.communicationService.getError();
    }
  }
}
