import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HelpComponent} from "./help.component";
import {ChooseHelpComponent} from "./choose-help/choose-help.component";
import {ChooseVideoComponent} from "./choose-video/choose-video.component";
import {VideoComponent} from "./video/video.component";


const routes: Routes = [
  {
    path: 'help',
    component: HelpComponent,
    children: [
      {
        path: '',
        component: ChooseHelpComponent
      },
      {
        path: 'choose-video',
        component: ChooseVideoComponent
      },
      {
        path: 'video',
        component: VideoComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HelpRoutingModule { }
