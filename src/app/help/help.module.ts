import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HelpRoutingModule } from './help-routing.module';
import { HelpComponent } from './help.component';
import { ChooseHelpComponent } from './choose-help/choose-help.component';
import { ChooseVideoComponent } from './choose-video/choose-video.component';
import { VideoComponent } from './video/video.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import { SupportDialogComponent } from './support-dialog/support-dialog.component';
import {MatVideoModule} from "mat-video";


@NgModule({
  declarations: [HelpComponent, ChooseHelpComponent, ChooseVideoComponent, VideoComponent, SupportDialogComponent, SupportDialogComponent],
  imports: [
    CommonModule,
    HelpRoutingModule,
    FlexLayoutModule,
    MatButtonModule,
    MatDialogModule,
    MatVideoModule
  ],
  entryComponents: [SupportDialogComponent]
})
export class HelpModule { }
