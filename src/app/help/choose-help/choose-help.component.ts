import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {SupportDialogComponent} from "../support-dialog/support-dialog.component";

@Component({
  selector: 'help-choose-help',
  templateUrl: './choose-help.component.html',
  styleUrls: ['./choose-help.component.scss']
})
export class ChooseHelpComponent implements OnInit {

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(SupportDialogComponent, {
      height: '200px',
      width: '300px',
    });

    setTimeout(() => {
      dialogRef.close();
    }, 4000);
  }
}
