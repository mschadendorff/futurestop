import { Component, OnInit } from '@angular/core';
import {CommunicationService} from "../../services/communication.service";

@Component({
  selector: 'help-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {
  video: string;
  title: string;

  constructor(private communicationService: CommunicationService) { }

  ngOnInit() {
    this.video = this.communicationService.getVideo();
    if (this.video === 'routefinder') {
      this.title = "Tutorial Routenfinder";
    }
    else {
      this.title = "Tutorial Meine Umgebung";
    }
  }

}
