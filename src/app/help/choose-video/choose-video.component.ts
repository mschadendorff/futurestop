import { Component, OnInit } from '@angular/core';
import {CommunicationService} from "../../services/communication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'help-choose-video',
  templateUrl: './choose-video.component.html',
  styleUrls: ['./choose-video.component.scss']
})
export class ChooseVideoComponent implements OnInit {

  constructor(private communicationService: CommunicationService,
              private router: Router) { }

  ngOnInit() {}

  openVideo(type: string): void {
    this.communicationService.setVideo(type);
    this.router.navigate(["/help/video"]);
  }

}
