import { Injectable } from '@angular/core';
import {Provider} from "../destination/connections/model/provider";
import PointJson from '../../assets/data/lnglat.json';
import {Payment} from "./model/payment";

@Injectable({
  providedIn: 'root',
})
export class CommunicationService {
  private next: string = "";
  private start: number[];
  private destination: number[];
  private selectedProviders: Provider[] = [];
  private allProviders: Provider[];
  private error: string = "";
  private payMethod: string = "";
  private points: any[] = PointJson;
  private payments: Payment[];
  private video: string;

  constructor() {
    this.allProviders = [
      {
        key: 'ride_sharing',
        name: 'Ride Sharing',
        color: '#FF4136'
      },
      {
        key: 'scooter',
        name: 'E-Roller',
        color: '#7FDBFF'
      },
      {
        key: 'bus',
        name: 'Bus',
        color: '#3D9970'
      },
      {
        key: 'car_sharing',
        name: 'Car Sharing',
        color: '#FF851B'
      },
      {
        key: 'bike_sharing',
        name: 'Bike Sharing',
        color: '#FFDC00'
      },
      {
        key: 'walking',
        name: 'Gehen',
        color: '#2ECC40'
      }
      ];

    this.payments = [
        {
          id: "creditcard",
          method: "kontaktlose",
          link: "assets/img/payment_creditcard.png",
          instruction: "Bitte halte deine Geldkarte an das Feld"
        },
        {
          id: "barcode",
          method: "QR-Code",
          link: "assets/img/payment_barcode.png",
          instruction: "Bitte scanne den Code und schließe die Zahlung auf deinem Smartphone ab"
        }
      ]
  }

  getStart(): number[] {
    return this.start;
  }

  setStart(start: number[]) {
    this.start = start;
  }

  getDestination(): number[] {
    return this.destination;
  }

  setDestination(destination: number[]) {
    this.destination = destination;
  }

  setNext(next: string, ): void {
    this.next = next;
  }

  getNext(): string {
    return this.next;
  }

  getSelectedProviders(): Provider[] {
    return this.selectedProviders;
  }

  setSelectedProviders(selectedProviders: Provider[]) {
    this.selectedProviders = selectedProviders;
  }

  getAllProviders(walking: boolean = false): Provider[] {
    if (walking) {
      return this.allProviders.filter(x => x.key != 'walking');
    }

    return this.allProviders;
  }


  getError(): string {
    return this.error;
  }

  setError(error: string) {
    this.error = error;
  }


  getPayment(): Payment {
    return this.payments.find(elem => elem.id == this.payMethod);
  }

  setPayMethod(payMethod: string) {
    this.payMethod = payMethod;
  }

  getPoints(): any {
    return this.points;
  }


  getVideo(): string {
    return this.video;
  }

  setVideo(video: string) {
    this.video = video;
  }
}
