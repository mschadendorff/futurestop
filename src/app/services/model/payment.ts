export class Payment {
  id: string;
  method: string;
  link: string;
  instruction: string;
}
